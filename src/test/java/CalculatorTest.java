import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * décembre 2016
 *
 * @author Mirko Steinle
 */
public class CalculatorTest
{
    private FrameFixture window;

    @BeforeClass
    public static void setUpOnce() {
        FailOnThreadViolationRepaintManager.install();
    }

    /**
     * Le setup se passe ici.
     */
    @Before
    public void setUp() {
        window = new FrameFixture(GuiActionRunner.execute(Calculator::createAndShowGUI));
    }

    @Test
    public void shouldUpdateOutputWhenClickingButton() throws InterruptedException
    {
        window.button("1").click();
        window.label("output").requireText("1");
    }

    @Test
    public void shouldAddDigitsToEnd()
    {
        // exécution
        window.button("3").click();
        window.button("2").click();
        window.button("9").click();

        // vérification
        window.label("output").requireText("329");
    }

    @After
    public void tearDown() {
        window.cleanUp();
    }
}